package templatedata

import (
	"fmt"
	"html/template"
	"net/http"
	"strings"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// UserController ...
type UserController struct {
	session *mgo.Session
}

// NewUserController ...
func NewUserController(s *mgo.Session) *UserController {
	return &UserController{s}
}

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("templatedata/*"))
}

// CreateProduct ...
func (uc *UserController) CreateProduct(w http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodPost {
		if err := req.ParseForm(); err != nil {
			panic(err)
		}
		productName := req.FormValue("productname")
		productDesc := req.FormValue("productdesc")
		productPrice := req.FormValue("productprice")
		productimage := req.FormValue("productimage")

		p := ProductData{productName, productDesc, productPrice, productimage, ""}
		p.ID = bson.NewObjectId()
		uc.session.DB("products").C("productsInfo").Insert(p)

	}
	tpl.ExecuteTemplate(w, "productinsert.gohtml", nil)
}

// GetProduct ...
func (uc UserController) GetProduct(w http.ResponseWriter, req *http.Request) {

	var u = []ProductData{}
	if err := uc.session.DB("products").C("productsInfo").Find(nil).All(&u); err != nil {
		w.WriteHeader(404)
	}
	tpl.ExecuteTemplate(w, "productget.gohtml", u)
}

// SearchProduct ...
func (uc UserController) SearchProduct(w http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodPost {
		id := req.FormValue("id")

		if !bson.IsObjectIdHex(id) {
			w.WriteHeader(http.StatusNotFound) // 404
			return
		}

		oid := bson.ObjectIdHex(id)

		var u = ProductData{}

		if err := uc.session.DB("products").C("productsInfo").FindId(oid).One(&u); err != nil {
			// w.WriteHeader(404)
			w.Header().Set("Content-Type", "text/html;charset=utf-8")
			fmt.Fprint(w, "No Data Found")
		}
		tpl.ExecuteTemplate(w, "search.gohtml", u)
	}
}

var cartslice []bson.ObjectId

// AddCartProduct ...
func (uc UserController) AddCartProduct(w http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodPost {
		if err := req.ParseForm(); err != nil {
			panic(err)
		}
		w.Header().Set("Content-Type", "text/html;charset=utf-8")
		productid := req.FormValue("id")
		newid := GetHexFromString(productid)
		oid := bson.ObjectIdHex(newid)
		cartslice = append(cartslice, oid)
		u := []ProductData{}
		for i := 0; i < len(cartslice); i++ {
			var datas = ProductData{}
			if err := uc.session.DB("products").C("productsInfo").FindId(cartslice[i]).One(&datas); err != nil {
				// w.WriteHeader(404)
				w.Header().Set("Content-Type", "text/html;charset=utf-8")
				fmt.Fprint(w, "No Data Found")
			}
			u = append(u, datas)
		}

		tpl.ExecuteTemplate(w, "addcart.gohtml", u)
	}
	if req.Method == http.MethodGet {
		if err := req.ParseForm(); err != nil {
			panic(err)
		}
		w.Header().Set("Content-Type", "text/html;charset=utf-8")
		u := []ProductData{}
		for i := 0; i < len(cartslice); i++ {
			var datas = ProductData{}
			if err := uc.session.DB("products").C("productsInfo").FindId(cartslice[i]).One(&datas); err != nil {
				// w.WriteHeader(404)
				w.Header().Set("Content-Type", "text/html;charset=utf-8")
				fmt.Fprint(w, "No Data Found")
			}
			u = append(u, datas)
		}

		tpl.ExecuteTemplate(w, "addcart.gohtml", u)
	}

}

// GetHexFromString ...
func GetHexFromString(id string) string {
	trim1 := strings.Trim(id, "ObjectIdHex")
	trim2 := strings.Trim(trim1, ")")
	trim3 := strings.Trim(trim2, "(")
	trim4 := strings.Split(trim3, "\"")
	return trim4[1]
}

//Getproductforupate ...
func (uc UserController) Getproductforupate(w http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodPost {
		id := req.FormValue("updateid")

		if !bson.IsObjectIdHex(id) {
			w.WriteHeader(http.StatusNotFound) // 404
			return
		}
		oid := bson.ObjectIdHex(id)
		var u = ProductData{}
		if err := uc.session.DB("products").C("productsInfo").FindId(oid).One(&u); err != nil {
			// w.WriteHeader(404)
			w.Header().Set("Content-Type", "text/html;charset=utf-8")
			fmt.Fprint(w, "No Data Found")
		}

		tpl.ExecuteTemplate(w, "update.gohtml", u)
	}
}

// UpdateProduct ...
func (uc *UserController) UpdateProduct(w http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodPost {
		if err := req.ParseForm(); err != nil {
			panic(err)
		}
		productName := req.FormValue("updateproductname")
		productDesc := req.FormValue("updateproductdesc")
		productImage := req.FormValue("updateproductimage")
		productPrice := req.FormValue("updateproductprice")

		// fmt.Fprint(w, productName)

		recordID := req.FormValue("updateid")

		newid := GetHexFromString(recordID)
		oid := bson.ObjectIdHex(newid)
		//objID, _ := primitive.ObjectIDFromHex(newid)
		fmt.Print(recordID)
		fmt.Print(newid)
		fmt.Print(productName)

		p := ProductData{productName, productDesc, productPrice, productImage, oid}

		if err := uc.session.DB("products").C("productsInfo").UpdateId(oid, p); err != nil {
			// w.WriteHeader(404)
			w.Header().Set("Content-Type", "text/html;charset=utf-8")
			fmt.Fprint(w, "No Data Found")
		}
		tpl.ExecuteTemplate(w, "updatedata.gohtml", nil)
	}
}
