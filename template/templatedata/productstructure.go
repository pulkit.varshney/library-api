package templatedata

import "gopkg.in/mgo.v2/bson"

// ProductData ...
type ProductData struct {
	ProductName  string        `json:"productname" bson:"productname"`
	ProductDesc  string        `json:"productdesc" bson:"productdesc"`
	ProductPrice string        `json:"productprice" bson:"productprice"`
	ProductImage string        `json:"productimage" bson:"productimage"`
	ID           bson.ObjectId `json:"id" bson:"_id"`
}
