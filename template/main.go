package main

import (
	"code/webprograms/second_web/mongodb/template/templatedata"

	"net/http"

	"gopkg.in/mgo.v2"
)

func main() {
	uc := templatedata.NewUserController(getSession())
	http.Handle("/resources/", http.StripPrefix("/resources", http.FileServer(http.Dir("./assets"))))
	http.HandleFunc("/createproduct", uc.CreateProduct)
	http.HandleFunc("/getproduct", uc.GetProduct)
	http.HandleFunc("/search", uc.SearchProduct)
	http.HandleFunc("/addtocart", uc.AddCartProduct)
	http.HandleFunc("/update", uc.Getproductforupate)
	http.HandleFunc("/updated", uc.UpdateProduct)
	http.ListenAndServe("localhost:8080", nil)
}
func getSession() *mgo.Session {
	s, err := mgo.Dial("mongodb://localhost")

	if err != nil {
		panic(err)
	}
	return s
}
