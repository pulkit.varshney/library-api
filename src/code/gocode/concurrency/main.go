package main

import "fmt"

func first(n int) {
	for i := 0; i < 10; i++ {
		fmt.Println("First", i)
	}
}
func second(n int) {
	for i := 0; i < 5; i++ {
		fmt.Println("second", i)
	}
}

func main() {
	go first(0)
	second(0)
	var input string
	fmt.Scanln(&input)
}
