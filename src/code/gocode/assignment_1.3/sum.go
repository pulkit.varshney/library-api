package main

import (
	"fmt"
)

var arr [5]int

func sum() (result int) {
	for num := 0; num < len(arr); num++ {
		result += arr[num]
	}
	return
}
func main() {
	fmt.Println("Enter five numbers")

	for num := 0; num < 5; num++ {
		fmt.Scanln(&arr[num])
	}

	fmt.Println("sum of the numbers is :=", sum())

}
