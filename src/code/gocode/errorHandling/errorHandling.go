package main

import (
	"fmt"
)

func main() {

	//Open a file
	// f, err := os.Open("filename.ext")
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// fmt.Println(f)

	//Printing a value

	// n, err := fmt.Println("Pulkit")
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// fmt.Println(n)

	//creating a file

	// f, err := os.Create("File.txt")
	// if err != nil {
	// 	fmt.Print(err)
	// 	return
	// }
	// defer f.Close()

	// r := strings.NewReader("Pulkit Varshney")
	// io.Copy(f, r)

	//reading a file
	// f, err := os.Open("File.txt")
	// if err != nil {
	// 	fmt.Print(err)
	// 	return
	// }
	// defer f.Close()
	// b, err := ioutil.ReadAll(f)
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// fmt.Print(string(b))

	// _, err := os.Open("nofile.txt")
	// if err != nil {
	// 	fmt.Println("error happened", err)
	// 	log.Println("error happened", err)
	// 	log.Fatalln("error happened", err)
	// 	panic(err)
	// 	return
	// }

	//errors.New

	// 	_, err := fmt.Println(sqrt(-10))
	// 	if err != nil {
	// 		log.Fatalln(err)
	// 	}
	// }

	// func sqrt(num float64) (float64, error) {
	// 	if num < 0 {
	// 		return 0, errors.New("can not do the square root of negative and zero number")
	// 	}
	// 	return math.Sqrt(num), nil
	// }

	//errorof

	// _, err := sqrt(-10)
	// if err != nil {
	// 	log.Fatalln(err)
	// }
	var num int
	fmt.Println("enter number")
	fmt.Scan(&num)
	fmt.Println("the number is :", num)
	type num1 struct{}
}

// func sqrt(num float64) (float64, error) {
// 	if num < 0 {
// 		return 0, fmt.Errorf("can not do the square root of negative and zero number")
// 	}
// 	return math.Sqrt(num), nil
// }
