package main

import (
	"fmt"
)

func zero(y *int) {
	*y = 10
	return
}

func main() {
	x := 20
	fmt.Println(x)
	zero(&x)
	fmt.Println(&x)
	fmt.Println(x)

	y := new(int)
	fmt.Println(y)
	zero(y)
	fmt.Println(*y)
	fmt.Println(y)
	name := "pulkit"
	greeting := "Hello"
	greetings(&name, &greeting)
	fmt.Println(name)
}
func greetings(name *string, greeting *string) {
	fmt.Println(*greeting, " ", *name)
	*name = "jawe"
}
