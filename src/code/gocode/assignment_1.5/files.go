package main

import (
	"fmt"
	"os"
)

var path = "NOTES.txt"

func main() {
	creating()
	writing()
	display()
}
func creating() {
	_, err := os.Create(path)
	if err != nil {
		fmt.Println(err)
		return
	}

}
func writing() {

	var file, err = os.OpenFile(path, os.O_WRONLY, 0644)
	if err != nil {
		return
	}
	defer file.Close()
	for i := 1; i <= 100; i++ {
		_, err = file.WriteString(fmt.Sprintf("%d\n", i))
		if err != nil {
			return
		}
	}
}
func display() {
	var file, err = os.OpenFile(path, os.O_RDONLY, 0644)
	if err != nil {
		return
	}
	defer file.Close()
	var data = make([]byte, 1024)
	_, err = file.Read(data)
	fmt.Println("Reading from file.")
	fmt.Println(string(data))
}
