package main

import "fmt"

func main() {
	n := 9
	for i := 0; i < n; i++ {
		fmt.Print(" ", fibo(i))
	}
}
func fibo(n int) int {
	if n == 0 {
		return 0
	}
	if n == 1 || n == 2 {
		return 1
	}
	// if n == 2 {
	// 	return 1
	// }
	return fibo(n-1) + fibo(n-2)
}
