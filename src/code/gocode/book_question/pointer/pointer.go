package main

import "fmt"

func main() {
	// 	num := 10
	// 	a := &num
	// 	num1 := new(int)
	// 	*num1 = 10
	// 	b := &num
	// 	fmt.Println(num, " ", a)
	// 	fmt.Print(*b)
	a := 1
	b := 2
	fmt.Println(a, b)
	swap(&a, &b)
	fmt.Println(a, b)
	fmt.Println(&a, &b)
}
func swap(a *int, b *int) {
	var temp int
	fmt.Println(*a, *b)
	fmt.Println(&a, &b)
	temp = *a
	*a = *b
	*b = temp
}
