package main

import "fmt"

func main() {
	var add int
	x := []int{1, 2, 3, 4, 1, 2, 3, 1, 3, 2}
	num := x[0:5]
	add = sum(num)
	fmt.Println(add)
}
func sum(num []int) (add int) {

	for i := 0; i < len(num); i++ {
		add = add + num[i]
	}
	return
}
