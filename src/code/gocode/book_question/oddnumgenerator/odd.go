package main

import (
	"fmt"
)

func main() {
	result := oddnumgenerator()
	fmt.Println(result())
	fmt.Println(result())
	fmt.Println(result())
}
func oddnumgenerator() func() int {
	i := 1
	return func() (ret int) {
		ret = i
		i += 2
		return
	}
}
