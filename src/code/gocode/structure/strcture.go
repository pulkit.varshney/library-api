package main

import (
	"fmt"
	"math"
)

//Circle := this is circle
type Circle struct {
	x, y, r float64
}

//Rectangle := this is rectangle
type Rectangle struct {
	x1, y1, x2, y2 float64
}

//Person := this is a Person
type Person struct {
	name string
}

func (p *Person) talk() {
	fmt.Println("my name is ", p.name)
}

//Android := this is android
type Android struct {
	Person
}

func main() {
	first := Circle{x: 0, y: 0, r: 5}
	second := Circle{0, 0, 2}
	fmt.Println(math.Pi * first.r * first.r)
	fmt.Println(math.Pi * second.r * second.r)
	fmt.Println(circleArea(&first))
	fmt.Println(first.Area())
	rec := Rectangle{2, 4, 5, 8}
	fmt.Println(rec.RectangleArea())
	per := Person{"Pulkit"}
	per.talk()
	roid := new(Person)
	roid.name = "jawe"
	roid.talk()
}
func circleArea(c *Circle) float64 {
	return math.Pi * c.r * c.r

}

//Area ...
func (c *Circle) Area() float64 {
	return math.Pi * c.r * c.r
}

//RectangleArea ...
func (r *Rectangle) RectangleArea() float64 {
	l := distance(r.x1, r.y1, r.x1, r.y2)
	w := distance(r.x1, r.y1, r.x2, r.y1)
	return l * w
}
func distance(x1, y1, x2, y2 float64) float64 {
	a := x2 - x1
	b := y2 - y1
	return math.Sqrt(math.Pow(a, 2) + math.Pow(b, 2))
}
