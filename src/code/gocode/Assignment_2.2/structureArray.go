package main

import (
	"fmt"
)

//Employee ...
type Employee struct {
	id      int
	name    string
	address string
}

//Structure of employee array
var emps []Employee

//main methiod
func main() {
	getEmployee()
	fmt.Println()
	for num := 0; num < len(emps); num++ {
		emps[num].printEmployee()
	}
}

//printing employee details
func (employee *Employee) printEmployee() {
	fmt.Println("/************************************/")
	fmt.Println("\t\t", employee.id)
	fmt.Println("\nId: ", employee.id, "\nName: ", employee.name, "\nAddress: ", employee.address, "\n")
	fmt.Println("/************************************/")
}

//getting employee details
func getEmployee() {
	var (
		id      int
		name    string
		address string
		num     int
		data    Employee
	)
	fmt.Println("How many data of employees you want to enter")
	fmt.Scanln(&num)
	for loop := 0; loop < num; loop++ {
		fmt.Println("Enter Id: ")
		fmt.Scanln(&id)
		fmt.Println("Enter Name: ")
		fmt.Scanln(&name)
		fmt.Println("Enter Address: ")
		fmt.Scanln(&address)
		data = Employee{id, name, address}
		emps = append(emps, data)
	}
}
