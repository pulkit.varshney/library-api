package main

import (
	"code/gocode/unittesting/testingfolder"
	"fmt"
	"testing"
)

func TestAbs(t *testing.T) {
	got := 10 + 20
	if got != 20 {
		t.Error("want 30 found 20")
	}
}

func TestAverage(t *testing.T) {
	got := testingfolder.Add(3, 3, 5, 5)
	if got == 0 {
		t.Error("Average Function is not working", got)
	}
	fmt.Println(got)
}
