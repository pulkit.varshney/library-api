// package main

// import "fmt"

// func main() {
// 	// xs := [10]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
// 	// avg(xs)

// 	// lx := []float64{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
// 	// fmt.Println(average(lx))

// 	// var r int
// 	// x := f()
// 	// fmt.Print(x)

// 	// fmt.Println(add(1, 2, 3))

// 	// incre()

// 	// fmt.Print(fact(5))

// 	// for i := 0; i < 5; i++ {
// 	// 	defer fmt.Printf("%d ", i)
// 	// }

// 	defer second()
// 	first()

// }

// // func avg(xs [10]int) {
// // 	total := 0
// // 	for _, value := range xs {
// // 		// fmt.Print(" ", index, value, " ")
// // 		total += value

// // 	}
// // 	fmt.Println(total)
// // }

// // func average(lx []float64) float64 {
// // 	total := 0.0
// // 	for _, v := range lx {
// // 		total += v
// // 	}
// // 	return total / float64(len(lx))
// // }

// ///////////////Named return type/////////////////
// // func f() (r int) {
// // 	r = 1
// // 	return
// // }

// /////////////////varargs//////////////////

// // func add(args ...int) int {
// // 	total := 0
// // 	for _, v := range args {
// // 		total += v
// // 	}
// // 	return total
// // }

// ////////////////////closure///////////////

// // func incre() {
// // 	x := 1
// // 	inc := func(x int) int {
// // 		x++
// // 		return x
// // 	}
// // 	fmt.Print(inc(x))
// // }

// ////////////////////Recursion/////////////////

// // func fact(num int) int {
// // 	if num == 0 {
// // 		return 1
// // 	}
// // 	return num * fact(num-1)
// // }

// ////////////////////Defer////////////////
// func first() {
// 	fmt.Println("First")
// }
// func second() {
// 	fmt.Println("Second")
// }
