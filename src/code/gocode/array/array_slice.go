package main

import (
	"fmt"
)

func main() {
	// x := []int{0, 1, 2, 3, 4}
	// var total int = 0
	// for i := 0; i < len(x); i++ {
	// 	total += x[i]
	// }
	// fmt.Print(total)
	// fmt.Println()
	// var y [5]int
	// fmt.Print("enter the number")
	// for i := 0; i < 5; i++ {
	// 	fmt.Scanf("%d", &y[i])

	// }
	// fmt.Println()
	// for i := 0; i < len(x); i++ {
	// 	fmt.Print(x[i], " ")

	// }
	// fmt.Println()
	// arr := []int{12, 43, 1, 54, 3}
	// for i := range arr {
	// 	fmt.Print(arr[i], " ")
	// }
	// fmt.Println()
	// for i := range arr {
	// 	fmt.Print(arr[i], " ")
	// }
	// fmt.Println()
	//////////////////slice///////////////////////////
	numbers := []int{1, 2, 3, 4, 5, 6, 778, 4}
	slice1 := numbers[0:5]
	fmt.Println(slice1)
	slice1 = append(slice1, 2, 4, 5, 56, 7, 89, 9, 7)
	fmt.Println(slice1)
	fmt.Println(numbers)
	slice2 := make([]int, 2)                                      
	copy(slice2, slice1)
	fmt.Print(slice2)
}
