package main

import (
	"fmt"
)

func main() {

	for i := 1; i < 6; i++ {
		for j := 6; j > i; j-- {
			fmt.Print(" ")
		}
		for k := i; k >= 1; k-- {
			fmt.Print(k)

		}
		for l := 2; l <= i; l++ {
			fmt.Print(l)
		}

		fmt.Println()
	}
}
