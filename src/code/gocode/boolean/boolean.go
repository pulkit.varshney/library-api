package main

import (
	"fmt"
)

func main() {
	fmt.Println(true && true)
	fmt.Println(true && false)
	fmt.Println(true || true)
	fmt.Println(true || false)
	fmt.Println(!true)
	var x string = "hello"
	var y string = "hello"
	fmt.Println(x == y)
	z := "hello"
	fmt.Print(z)
}
