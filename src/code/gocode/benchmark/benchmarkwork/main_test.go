package benchmarkwork

import (
	"fmt"
	"testing"
)

//TestGreet ...
func TestGreet(t *testing.T) {
	s := Greet("Pulkit")
	if s != "Pulkit" {
		t.Error("got", s, "Expected Welcome Pulkit")
	}
}
func ExampleGreet() {
	fmt.Println(Greet("Pulkit"))
}

// Output:
// Welcome Pulkit

func benchmarkGreet(b *testing.T) {
	for i := 0; i < 100000000; i++ {
		Greet("Pulkit")
	}
}
