package main

import (
	"fmt"
	"math"
)

// 1. question
// type vowelFinder interface {
// 	findVowels() []rune
// }

// //MyString := thi is  a string
// type MyString string

// func (ms MyString) findVowels() []rune {
// 	var vowels []rune
// 	for _, rune := range ms {
// 		if rune == 'a' || rune == 'e' || rune == 'i' || rune == 'o' || rune == 'u' {
// 			vowels = append(vowels, rune)
// 		}
// 	}
// 	return vowels
// }
// func main() {
// 	name := MyString("Pulkit Varshney")
// 	var v vowelFinder
// 	v = name
// 	fmt.Printf("Vowels are %c", v.findVowels())
// }

// 2. question

// //Person ...
// type Person struct {
// 	first string
// 	last  string
// }

// //Agent ...
// type Agent struct {
// 	Person
// 	ltk bool
// }

// //Human...
// type human interface {
// 	talk()
// }

// func (p Person) talk() {
// 	fmt.Println("My name is :", p.first, " ", p.last, " - Person")
// }
// func (p Agent) talk() {
// 	fmt.Println("My name is :", p.first, " ", p.last, " -Secret Agent")
// }
// func bar(h human) {
// 	fmt.Println("I was passes in bar", h)
// 	switch h.(type) {
// 	case Person:
// 		fmt.Println("I was passes in bar", h.(Person).first)
// 	case Agent:
// 		fmt.Println("I was passes in bar", h.(Agent).first)
// 	}
// }
// func main() {
// 	sal1 := Person{"Pulkit", "Varshney"}
// 	sal2 := Agent{
// 		Person: Person{"Palash", "Gupta"}, ltk: true,
// 	}
// 	fmt.Println(sal1)
// 	fmt.Println(sal2)
// 	sal1.talk()
// 	sal2.talk()
// 	bar(sal1)
// 	bar(sal2)

// 3. question

//Circle ...
type Circle struct {
	radius float64
}

//Rectangle ...
type Rectangle struct {
	length  float64
	breadth float64
}

//Perimeter ...
type Perimeter interface {
	valueTaken()
}

func (c Circle) valueTaken() {
	fmt.Println("the radius is :", c.radius)
}
func (c Rectangle) valueTaken() {
	fmt.Println("the length & breadth is :", c.length, " ", c.breadth)
}

//FindPerimeter ...
func FindPerimeter(p Perimeter) {
	switch p.(type) {
	case Circle:
		fmt.Println("Perimeter of Circle is", math.Pi*2*p.(Circle).radius)
	case Rectangle:
		fmt.Println("Perimeter of Rectangle is", 2*(p.(Rectangle).length+p.(Rectangle).breadth))
	}
}
func main() {
	circle := Circle{10}
	FindPerimeter(circle)
	rectangle := Rectangle{10, 20}
	FindPerimeter(rectangle)
}
