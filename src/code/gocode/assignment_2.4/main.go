package main

import (
	"fmt"

	"code/gocode/assignment_2.4/operations"
)

// "code/gocode/assignment_2.3/operations"
func main() {
	fmt.Printf("\n 1. Addition \n 2. Subtraction \n 3. Multiplication \n 4. Divide \n")
	var result int
	fmt.Scanln(&result)
	switch result {
	case 1:
		fmt.Println("Addition is := ", operations.Add(1.0, 2.23, 3.2324, 4.356, 5.356))
	case 2:
		fmt.Println("Subtraction is := ", operations.Subtract(1.0, 2.23, 3.2324, 4.356, 5.356))
	case 3:
		fmt.Println("Multiplication is := ", operations.Multiply(3.2324, 4.356, 5.356))
	case 4:
		fmt.Println("Divide is := ", operations.Divide(234.356, 5.356))
	}

}
