package operations

//Add ...
func Add(args ...float64) float64 {
	total := 0.0
	for _, v := range args {
		total += v
	}
	return total
}

//Subtract ...
func Subtract(args ...float64) float64 {
	total := 0.0
	for _, v := range args {
		total -= v
	}
	return total
}

//Multiply ...
func Multiply(args ...float64) float64 {
	total := 1.0
	for _, v := range args {
		total *= v
	}
	return total
}

//Divide ...
func Divide(a float64, b float64) float64 {
	return a / b
}
