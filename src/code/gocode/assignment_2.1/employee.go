package main

import (
	"fmt"
)

//Employee ...
type Employee struct {
	id      int
	name    string
	mobile  int64
	address string
}

func (e *Employee) empdetatils() {
	fmt.Println("\n Id :", e.id, "\n Name :", e.name, "\n Mobile_no :", e.mobile, "\n Address :", e.address)
}
func main() {

	//Implementation 1

	fmt.Println("This is One type of Implementation")
	var employee1 = Employee{1, "Pulkit", 8938905154, "Vaishali nagar Jaipur"}
	var employee2 = Employee{2, "Palash", 8790879654, "Vaishali nagar Jaipur"}
	var employee3 = Employee{3, "Nitin", 7906651105, "Vaishali nagar Jaipur"}
	var employee4 = Employee{4, "Shivani", 9089089876, "Vaishali nagar Jaipur"}
	var employee5 = Employee{5, "Reenu", 789098790, "Vaishali nagar Jaipur"}
	fmt.Println("\n", employee1, "\n", employee2, "\n", employee3, "\n", employee4, "\n", employee5)

	//***********************************************//

	//Implementation 2

	fmt.Println("\n\nThis is Second type of Implementation")
	var emp1 = Employee{1, "Pulkit", 8938905154, "Vaishali nagar Jaipur"}
	emp1.empdetatils()
	var emp2 = Employee{2, "Palash", 8790879654, "Vaishali nagar Jaipur"}
	emp2.empdetatils()
	var emp3 = Employee{3, "Nitin", 7906651105, "Vaishali nagar Jaipur"}
	emp3.empdetatils()
	var emp4 = Employee{4, "Shivani", 9089089876, "Vaishali nagar Jaipur"}
	emp4.empdetatils()
	var emp5 = Employee{5, "Reenu", 789098790, "Vaishali nagar Jaipur"}
	emp5.empdetatils()

	//**************************************//
}
