package main

import (
	"fmt"
)

var array [6]int

func findDifference() {
	var temp int
	for num := 0; num < len(array); num++ {
		for num2 := num + 1; num2 < len(array); num2++ {
			if array[num] > array[num2] {
				temp = array[num]
				array[num] = array[num2]
				array[num2] = temp
			}
		}
	}
	fmt.Println()
	length := len(array) - 1
	fmt.Println(array[length] - array[0])
}
func main() {
	fmt.Println("enter the 6 numbers")
	for num := 0; num < len(array); num++ {
		fmt.Scanln(&array[num])
	}
	findDifference()

}
