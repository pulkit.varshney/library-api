package main

import (
	"fmt"
)

func main() {
	var str = `hello,world`
	fmt.Print((str[3]))
	fmt.Println()
	var char string
	char = string(str[3])
	fmt.Print(char)
	fmt.Println()
	fmt.Print("hello" + "worlds")
}
