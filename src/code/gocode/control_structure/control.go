package main

import (
	"fmt"
)

func main() {
	for i := 1; i <= 10; i++ {
		fmt.Print(i, " ")
	}
	fmt.Println()
	for i := 1; i < 100; i++ {
		if i%2 == 0 {
			fmt.Println(i, "Even number")
		} else {
			fmt.Println(i, "Odd number")
		}
	}
	fmt.Println()
	arr := []int{12, 43, 1, 54, 3}
	for i := range arr {
		fmt.Print(arr[i], " ")
	}
	fmt.Println()
	var i int = 3
	if i == 0 {
		fmt.Println("Zero")
	} else if i == 1 {
		fmt.Println("One")
	} else if i == 2 {
		fmt.Println("Two")
	} else if i == 3 {
		fmt.Println("Three")
	} else if i == 4 {
		fmt.Println("Four")
	} else if i == 5 {
		fmt.Println("Five")
	}
}
