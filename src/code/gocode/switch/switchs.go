package main

import (
	"fmt"
)

func main() {
	i := 'a'
	switch i {
	case 'a':
		fmt.Println(int(i))
	case 'b':
		fmt.Println(int(i))
	case 'c':
		fmt.Println(int(i))
	case 'd':
		fmt.Println(int(i))
	case 'e':
		fmt.Println(int(i))
	default:
		fmt.Println("don't know")
	}
}
