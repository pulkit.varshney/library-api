package main

import "fmt"

var array = make([]int, 0)

func main() {
	var num int
	var enterValue int
	fmt.Println("Enter the No. of elements ")
	fmt.Scanln(&num)
	fmt.Println("Enter the elements")

	for i := 0; i < num; i++ {
		fmt.Scanln(&enterValue)
		array = append(array, enterValue)
	}
	fmt.Println("\nSum of the array is : ", sum())
	fmt.Println("Average of the array is : ", average())
	sorting()

}
func sum() (result int) {
	result = 0
	for num := 0; num < len(array); num++ {
		result += array[num]
	}
	return result
}
func average() (avg int) {
	arraySum := sum()
	avg = arraySum / len(array)
	return avg

}
func sorting() {
	var temp int
	for num := 0; num < len(array); num++ {
		for num2 := num + 1; num2 < len(array); num2++ {
			if array[num] > array[num2] {
				temp = array[num]
				array[num] = array[num2]
				array[num2] = temp
			}
		}
	}
	fmt.Println("Array After Sorting : ", array)
}
