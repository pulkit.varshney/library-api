package main

import (
	"fmt"
	"math"
)

//Circle ...
type Circle struct {
	radius float64
}

//Rectangle ...
type Rectangle struct {
	length  float64
	breadth float64
}

//Perimeter ...
type Perimeter interface {
	valueTaken()
}

//Value taken from circle structure
func (c Circle) valueTaken() {
	fmt.Println("the radius is :", c.radius)
}

//Value taken from rectangle structure
func (c Rectangle) valueTaken() {
	fmt.Println("the length & breadth is :", c.length, " ", c.breadth)
}

//FindPerimeter ...
func FindPerimeter(p Perimeter) {
	switch p.(type) {
	case Circle:
		fmt.Println("Perimeter of Circle is", math.Pi*2*p.(Circle).radius)
	case Rectangle:
		fmt.Println("Perimeter of Rectangle is", 2*(p.(Rectangle).length+p.(Rectangle).breadth))
	}
}
func main() {
	circle := Circle{10}
	FindPerimeter(circle)
	rectangle := Rectangle{10, 20}
	FindPerimeter(rectangle)
}
