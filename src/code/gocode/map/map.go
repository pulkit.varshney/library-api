package main

func main() {
	// x := make(map[string]int)
	// x["key"] = 10
	// fmt.Println(x["key"])

	// x := map[string]int{"key": 12, "value": 19}
	// fmt.Println(x)

	// x := map[string]int{}
	// x["kek"] = 10
	// fmt.Print(x)
	// i, ok := x["kek"]
	// fmt.Println(i, " ", ok)
	// _, okay := x["kek"]
	// fmt.Println(" ", okay)

	// elements := make(map[string]string)
	// elements["H"] = "Hydrogen"
	// elements["He"] = "Helium"
	// elements["Li"] = "Lithium"
	// elements["Be"] = "Beryllium"
	// elements["B"] = "Boron"
	// elements["C"] = "Carbon"
	// elements["N"] = "Nitrogen"
	// elements["O"] = "Oxygen"
	// elements["F"] = "Fluorine"
	// elements["Ne"] = "Neon"
	// fmt.Println(elements["Li"])
	// fmt.Println(elements["Un"])
	// if i, ok := elements["Li"]; ok {
	// 	fmt.Println(i, " ", ok)

	// }

	elements := map[string]map[string]string{
		"H": map[string]string{
			"name":  "Hydrogen",
			"state": "gas",
		},
		"He": map[string]string{
			"name":  "Helium",
			"state": "gas",
		},
		"Li": map[string]string{
			"name":  "Lithium",
			"state": "solid",
		},
		"Be": map[string]string{
			"name":  "Beryllium",
			"state": "solid",
		},
	}
	if el, ok := elements["Li"]; ok {
		fmt.Println(el["name"], el["state"])
	}
	fmt.Println(elements["H"])
	// var x [15]int
	number := [6]string{"a", "b", "c", "d", "e", "f"}
	x := number[2:5]
	fmt.Print(x)
	y := make([]int, 3, 6)
	fmt.Print(len(x), len(y))

}
