package librarystructure

// DeleteBook ...
type DeleteBook struct {
	Name       string
	AuthorName string
	Price      string
	Category   string
}

//DeleteBookkey ...
type DeleteBookkey struct {
	Category string
	Name     string
}

// DeleteBookdata ...
type DeleteBookdata struct {
	Name       string
	AuthorName string
	Price      string
	Category   string
}
