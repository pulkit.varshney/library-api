package librarystructure

//GetBookkey ...
type GetBookkey struct {
	Name string
}

//GetBooks ...
type GetBooks struct {
	Name       string
	AuthorName string
	Price      string
	Category   string
}

//GetBooksupdate ...
type GetBooksupdate struct {
	Name       string
	AuthorName string
	Price      string
	Category   string
}
