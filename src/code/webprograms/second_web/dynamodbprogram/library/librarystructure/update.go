package librarystructure

//Bookkey ...
type Bookkey struct {
	Category string
	Name     string
}

//Books ...
type Books struct {
	Name       string
	AuthorName string
	Price      string
	Category   string
}

//Booksupdate ...
type Booksupdate struct {
	Name       string
	AuthorName string
	Price      string
	Category   string
}
