package librarystructure

//Book ...
type Book struct {
	Name       string
	AuthorName string
	Price      string
	Category   string
}
