package libraryhandler

import (
	"bytes"
	"log"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
)

func getSession() *session.Session {
	sess, err := session.NewSession(&aws.Config{
		Endpoint:    aws.String("http://localhost:8000"),
		Region:      aws.String("eu-west-2"),
		Credentials: credentials.NewStaticCredentials("123", "123", ""),
	})

	if err != nil {
		panic(err)
	}
	return sess
}

func TestGetEntry(t *testing.T) {
	req, err := http.NewRequest("GET", "/entry", nil)
	if err != nil {
		log.Fatal(err)
	}
	rr := httptest.NewRecorder()
	uc := NewUserController(getSession())
	handler := http.HandlerFunc(uc.GetAllData)
	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	expected := `{Engineering Mathematics G C goswamy 659 Study},
				{Beleive in yourself J Jagannath 949 Motivational},{The Magic of Thinking Big David Schettz 550 Motivational}`
	if strings.Compare(rr.Body.String(), expected) != 1 {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func TestGetName(t *testing.T) {
	req, err := http.NewRequest("GET", "/getdatabyName", nil)
	if err != nil {
		t.Fatal(err)
	}
	data := req.URL.Query()
	data.Add("Name", "The Magic of Thinking Big")
	data.Add("Category", "Motivational")
	req.URL.RawQuery = data.Encode()
	rr := httptest.NewRecorder()
	uc := NewUserController(getSession())
	handler := http.HandlerFunc(uc.GetDatabyName)
	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	expected := `{The Magic of Thinking Big David Schettz 550 Motivational}`
	if strings.Compare(rr.Body.String(), expected) != 1 {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}

}
func TestCreateEntry(t *testing.T) {

	var jsonStr = []byte(`{"Category":"funny","Name":"You will be happy","AuthorName":"Pulkit","Price":"1199"}`)

	req, err := http.NewRequest("POST", "/insert", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("content-type", "application/json")
	rr := httptest.NewRecorder()
	bc := NewUserController(getSession())
	handler := http.HandlerFunc(bc.InsertBook)
	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Wrong Status code returned returned %v want %v", status, http.StatusOK)
	}
	expected := `{"funny","You will be happy","Pulkit ","1199"}`

	if strings.Compare(rr.Body.String(), expected) == 1 {
		t.Errorf("Handler returned unexpected body :: got %v :: want %v", rr.Body.String(), expected)
	}

}
