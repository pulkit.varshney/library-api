package libraryhandler

import (
	"code/webprograms/second_web/dynamodbprogram/library/librarystructure"
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

// UserController ...
type UserController struct {
	session *session.Session
}

// NewUserController ...
func NewUserController(s *session.Session) *UserController {
	return &UserController{s}
}

//CreateBook ...
func (uc *UserController) CreateBook(w http.ResponseWriter, req *http.Request) {
	// books := librarystructure.Book{}
	svc := dynamodb.New(uc.session)
	input := &dynamodb.CreateTableInput{
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("Category"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("Name"),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("Category"),
				KeyType:       aws.String("HASH"),
			},
			{
				AttributeName: aws.String("Name"),
				KeyType:       aws.String("RANGE"),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(10),
			WriteCapacityUnits: aws.Int64(10),
		},
		TableName: aws.String("Books"),
	}
	result, err := svc.CreateTable(input)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Fprintln(w, result)

}

//InsertBook ...
func (uc *UserController) InsertBook(w http.ResponseWriter, req *http.Request) {
	var books = librarystructure.Book{}

	err := json.NewDecoder(req.Body).Decode(&books)
	if err != nil {
		fmt.Println("Could not decode the moviedata.json data", err.Error())
	}
	svc := dynamodb.New(uc.session)
	av, err := dynamodbattribute.MarshalMap(books)

	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String("Books"),
	}

	_, err = svc.PutItem(input)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Printf("We have inserted a new item!\n")
}

//UpdateBook ...
func (uc *UserController) UpdateBook(w http.ResponseWriter, req *http.Request) {
	// var books = librarystructure.Books{}
	var booksupdate = librarystructure.Booksupdate{}
	var key = librarystructure.Bookkey{}
	err := json.NewDecoder(req.Body).Decode(&booksupdate)
	if err != nil {

	}
	svc := dynamodb.New(uc.session)

	key.Category = booksupdate.Category
	key.Name = booksupdate.Name
	keys, err := dynamodbattribute.MarshalMap(librarystructure.Bookkey{
		Category: booksupdate.Category,
		Name:     booksupdate.Name,
	})

	input := &dynamodb.UpdateItemInput{
		Key:              keys,
		TableName:        aws.String("Books"),
		UpdateExpression: aws.String("set Price = :p , AuthorName=:r"),
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":r": {
				S: aws.String(booksupdate.AuthorName),
			},
			":p": {
				S: aws.String(booksupdate.Price),
			},
		},
		ReturnValues: aws.String("UPDATED_NEW"),
	}
	result, err := svc.UpdateItem(input)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	updatedAttributes := librarystructure.Books{}
	err = dynamodbattribute.UnmarshalMap(result.Attributes, &updatedAttributes)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

}

//DeleteBook ...
func (uc *UserController) DeleteBook(w http.ResponseWriter, req *http.Request) {
	var deletebook = librarystructure.DeleteBook{}
	var deletebookkey = librarystructure.DeleteBookkey{}
	err := json.NewDecoder(req.Body).Decode(&deletebook)
	if err != nil {

	}
	svc := dynamodb.New(uc.session)
	deletebookkey.Category = deletebook.Category
	deletebookkey.Name = deletebook.Name
	keys, err := dynamodbattribute.MarshalMap(librarystructure.Bookkey{
		Category: deletebook.Category,
		Name:     deletebook.Name,
	})

	if err != nil {
		fmt.Println(err.Error())
		return
	}
	input := &dynamodb.DeleteItemInput{
		Key:       keys,
		TableName: aws.String("Books"),
	}

	_, err = svc.DeleteItem(input)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

}

//GetAllData ...
func (uc *UserController) GetAllData(w http.ResponseWriter, req *http.Request) {
	svc := dynamodb.New(uc.session)
	tableName := "Books"
	proj := expression.NamesList(expression.Name("Category"), expression.Name("Name"), expression.Name("AuthorName"), expression.Name("Price"))

	expr, err := expression.NewBuilder().WithProjection(proj).Build()
	if err != nil {
		fmt.Println("Got error building expression:")
		fmt.Println(err.Error())
		os.Exit(1)
	}
	params := &dynamodb.ScanInput{
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ProjectionExpression:      expr.Projection(),
		TableName:                 aws.String(tableName),
	}
	result, err := svc.Scan(params)
	if err != nil {
		fmt.Println("Query API call failed:")
		fmt.Println((err.Error()))
		os.Exit(1)
	}

	for _, i := range result.Items {
		item := librarystructure.Book{}
		err = dynamodbattribute.UnmarshalMap(i, &item)
		if err != nil {
			fmt.Println("Got error unmarshalling:")
			fmt.Println(err.Error())
			os.Exit(1)
		}
		// fmt.Fprintln(w, item.Name)
		// fmt.Fprintln(w, item.AuthorName)
		// fmt.Fprintln(w, item.Category)
		// fmt.Fprintln(w, item.Price)
		fmt.Fprintf(w, "%v", item)
	}
}

//GetDatabyName ...
func (uc *UserController) GetDatabyName(w http.ResponseWriter, req *http.Request) {
	book := librarystructure.Book{}
	book.Name = req.FormValue("Name")
	book.Category = req.FormValue("Category")
	json.NewDecoder(req.Body).Decode(&book)
	svc := dynamodb.New(uc.session)
	//	av, err := dynamodbattribute.MarshalMap(b)
	result, err := svc.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String("Books"),
		Key: map[string]*dynamodb.AttributeValue{
			"Name": {

				S: aws.String(book.Name),
			},
			"Category": {
				S: aws.String(book.Category),
			},
		},
	})

	if err != nil {
		fmt.Println(err.Error())
		return
	}
	item := librarystructure.Book{}
	err = dynamodbattribute.UnmarshalMap(result.Item, &item)
	if err != nil {
		panic(fmt.Sprintf("Failed to unmarshal Record, %v", err))
	}

	if item.Name == "" {
		fmt.Println("Could not find name empty")
		fmt.Fprintln(w, "Could not find any value")
		return
	}
	fmt.Fprintf(w, "Name:  %v ", item.Name)
	fmt.Fprintf(w, "AuthorName: %v", item.AuthorName)
	fmt.Fprintf(w, "Category: %v", item.Category)
	fmt.Fprintf(w, "Price: %v", item.Price)
}
