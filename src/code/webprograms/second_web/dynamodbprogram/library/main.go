package main

import (
	"code/webprograms/second_web/dynamodbprogram/library/libraryhandler"
	"net/http"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
)

func main() {

	uc := libraryhandler.NewUserController(getSession())
	http.HandleFunc("/create", uc.CreateBook)
	http.HandleFunc("/insert", uc.InsertBook)
	http.HandleFunc("/update", uc.UpdateBook)
	http.HandleFunc("/delete", uc.DeleteBook)
	http.HandleFunc("/getalldata", uc.GetAllData)
	http.HandleFunc("/getdatabyName", uc.GetDatabyName)
	http.ListenAndServe("localhost:8080", nil)

}
func getSession() *session.Session {
	sess, err := session.NewSession(&aws.Config{
		Endpoint:    aws.String("http://localhost:8000"),
		Region:      aws.String("eu-west-2"),
		Credentials: credentials.NewStaticCredentials("123", "123", ""),
	})

	if err != nil {
		panic(err)
	}
	return sess
}
