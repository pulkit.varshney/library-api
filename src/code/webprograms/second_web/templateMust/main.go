package main

import (
	"log"
	"os"
	"text/template"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("templates/*"))
}
func main() {

	err := tpl.Execute(os.Stdout, "one")
	if err != nil {
		log.Fatalln(err)
	}
	// //
	// 	tpl, err = tpl.ParseFiles("templates/three.gohtml", "templates/two.gohtml")
	// 	if err != nil {
	// 		log.Fatalln(err)
	// 	}

	err = tpl.ExecuteTemplate(os.Stdout, "three.gohtml", "three")
	if err != nil {
		log.Fatalln(err)
	}

	err = tpl.ExecuteTemplate(os.Stdout, "one.gohtml", "one")
	if err != nil {
		log.Fatalln(err)
	}

	err = tpl.ExecuteTemplate(os.Stdout, "two.gohtml", " two")
	if err != nil {
		log.Fatalln(err)
	}

	err = tpl.Execute(os.Stdout, "one")
	if err != nil {
		log.Fatalln(err)
	}
}
