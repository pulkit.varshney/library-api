package main

import (
	"fmt"
	"os"
	"testing"

	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
)

// Define stub
type stubDynamoDB struct {
	dynamodbiface.DynamoDBAPI
}

type Record struct {
	ID   string
	URLs []string
}
type book struct {
	Category   string
	AuthorName string
	Price      string
	Name       string
}

func (m *stubDynamoDB) GetItem(input *dynamodb.GetItemInput) (*dynamodb.GetItemOutput, error) {

	items := []map[string]*dynamodb.AttributeValue{}

	mybook := book{
		Category: "Study",
		Name:     "G C goswamy",
	}

	av, err := dynamodbattribute.MarshalMap(mybook)
	if err != nil {
		fmt.Println("Got error marshalling new movie item:")
		fmt.Println(err.Error())
		os.Exit(1)
	}
	items = append(items, av)

	// val := dynamodb.AttributeValue{}
	// val.SetS("sample-val")
	// resp := make(map[string]*dynamodb.AttributeValue)
	// resp["key"] = &key
	// resp["val"] = &val

	// Returned canned response
	output := &dynamodb.GetItemOutput{
		Item: items[0],
	}
	return output, nil

}

//InsertItem ...
func (m *stubDynamoDB) InsertItem(input *dynamodb.GetItemInput) (*dynamodb.GetItemOutput, error) {

	items := []map[string]*dynamodb.AttributeValue{}

	mybook := book{
		Category:   "Study",
		AuthorName: "Nitin",
		Price:      "1000",
		Name:       "G C ",
	}

	av, err := dynamodbattribute.MarshalMap(mybook)
	if err != nil {
		fmt.Println("Got error marshalling new movie item:")
		fmt.Println(err.Error())
		os.Exit(1)
	}
	items = append(items, av)

	// val := dynamodb.AttributeValue{}
	// val.SetS("sample-val")
	// resp := make(map[string]*dynamodb.AttributeValue)
	// resp["key"] = &key
	// resp["val"] = &val

	// Returned canned response
	output := &dynamodb.GetItemOutput{
		Item: items[0],
	}
	return output, nil

}

// Sample Test Case

func TestDynamodb(t *testing.T) {
	svc := &stubDynamoDB{}
	res, err := callDynamodb(svc)
	if err != nil {
		t.Errorf("Error calling Dynamodb %d", err)
	}
	keyRes := *res.Item["Name"].S
	valRes := *res.Item["Category"].S
	if keyRes != "G C goswamy" {
		t.Errorf("Wrong key returned. Shoule be sample-key, was %s", keyRes)
	}
	if valRes != "Study" {
		t.Errorf("Wrong value returned. Shoule be sample-val, was %s", valRes)
	}
}
