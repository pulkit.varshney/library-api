package main

import (
	"log"
	"net/http"
	"text/template"
)

var tpl *template.Template

func index(res http.ResponseWriter, req *http.Request) {
	err := tpl.ExecuteTemplate(res, "index.gohtml", nil)
	if err != nil {
		log.Fatalln(err)
	}
}
func about(res http.ResponseWriter, req *http.Request) {
	err := tpl.ExecuteTemplate(res, "about.gohtml", nil)
	if err != nil {
		log.Fatalln(err)
	}
}
func init() {
	tpl = template.Must(template.ParseGlob("Templates/*"))
}
func main() {
	http.HandleFunc("/", index)
	http.HandleFunc("/about", about)
	http.ListenAndServe(":8080", nil)
}
