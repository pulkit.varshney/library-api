package main

import (
	"fmt"
	"net/http"
)

//Set ...
func Set(res http.ResponseWriter, req *http.Request) {
	http.SetCookie(res, &http.Cookie{
		Name:  "GoCookie",
		Value: "Jsut Log in",
	})
	fmt.Fprint(res, "Cookie is written in browser")
}

//Read ...
func Read(res http.ResponseWriter, req *http.Request) {
	f, err := req.Cookie("GoCookie")
	if err != nil {
		http.Error(res, err.Error(), http.StatusNotFound)
	}
	fmt.Fprintln(res, f)
}
func main() {
	http.HandleFunc("/", Set)
	http.HandleFunc("/read", Read)
	http.ListenAndServe(":8081", nil)
}
