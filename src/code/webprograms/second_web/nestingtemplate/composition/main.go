package main

import (
	"html/template"
	"log"
	"os"
)

var tpl *template.Template

func init(){
	tpl = template.Must(template.ParseFiles("index.gohtml"))
}

type course struct{
	Number,Name,Units string
}
type semester struct{
	Term string
	Courses []course
}
type year struct{
	Fall,Spring,Summer semester
	Value int
}
//function
func (y year)FindSquare()int{
	return (y.Value*y.Value)
}
//Pipelining function
func (y year)SquareRoot(x int)int{
	return (x*20)
}
func main(){
	first := year{
		Fall: semester{
			Term:"Fall",
			Courses: []course{
				course{"CS801","Compiler Designing","5"},
				course{"CS802","Application Designing","5"},
				course{"CS803","Android","5"},
			},
		},
		Value:10,
		Spring: semester{
			Term: "Spring",
			Courses:[]course{
				course{"CS801","Web","5"},
				course{"CS802","Product Designing","5"},
				course{"CS803","Kotlin","5"},
			},
		},
	}
	err := tpl.Execute(os.Stdout,first)
	if err!=nil{
		log.Fatalln(err)
	}
}