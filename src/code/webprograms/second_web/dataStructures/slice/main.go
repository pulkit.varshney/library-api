package main

import (
	"log"
	"os"
	"text/template"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("tpl.gohtml"))
}
func main() {

	// err := tpl.ExecuteTemplate(os.Stdout, "tpl.gohtml", `Golang is Good ,I am loving it`)
	data := []string{"Gandhi", "Rahul", "Sonia", "Varun", "Sanjay"}
	err := tpl.Execute(os.Stdout, data)
	if err != nil {
		log.Fatalln(err)
	}
}
