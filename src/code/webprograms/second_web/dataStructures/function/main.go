package main

import (
	"log"
	"os"
	"strings"
	"text/template"
)

var tpl *template.Template
var fm = template.FuncMap{
	"uc": strings.ToUpper,
	"ft": firstThree,
}

func init() {
	tpl = template.Must(template.New("").Funcs(fm).ParseFiles("index.gohtml"))
}

type legends struct {
	Name string
	Work string
}
type car struct {
	Name string
	Year int
}

// type complete struct {
// 	Wisdom    []legends
// 	Transport []car
// }

func firstThree(s string) string {
	s = strings.TrimSpace(s)
	if len(s) >= 3 {
		s = s[:3]
	}
	return s
}

func main() {
	gandhi := legends{
		Name: "Gandhi",
		Work: "Freedom Fighting",
	}
	buddha := legends{
		Name: "Buddha",
		Work: "make People disease fre",
	}
	jesus := legends{
		Name: "Jesus",
		Work: "make People kind",
	}
	gonvindji := legends{
		Name: "Gonvindji",
		Work: "like a power for others",
	}
	mohhamad := legends{
		Name: "Mohhamad",
		Work: "Make other life easy",
	}
	car1 := car{
		Name: "BMW",
		Year: 5,
	}
	car2 := car{
		Name: "Bantely",
		Year: 4,
	}
	legendsdata := []legends{gandhi, buddha, jesus, gonvindji, mohhamad}
	cardata := []car{car1, car2}
	data := struct {
		Wisdom    []legends
		Transport []car
	}{
		legendsdata, cardata,
	}
	// data := complete{Wisdom: legendsdata, Transport: cardata}
	err := tpl.ExecuteTemplate(os.Stdout, "index.gohtml", data)
	if err != nil {
		log.Fatal(err)
	}
}
