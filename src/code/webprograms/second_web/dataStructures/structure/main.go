package main

import (
	"log"
	"os"
	"text/template"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("index.gohtml"))
}

type legends struct {
	Name string
	Work string
}
type car struct {
	Name string
	Year int
}

// type complete struct {
// 	Wisdom    []legends
// 	Transport []car
// }

func main() {
	gandhi := legends{
		Name: "Gandhi",
		Work: "Freedom Fighting",
	}
	buddha := legends{
		Name: "Buddha",
		Work: "make People disease fre",
	}
	jesus := legends{
		Name: "Jesus",
		Work: "make People kind",
	}
	gonvindji := legends{
		Name: "Gonvindji",
		Work: "like a power for others",
	}
	mohhamad := legends{
		Name: "Mohhamad",
		Work: "Make other life easy",
	}
	car1 := car{
		Name: "BMW",
		Year: 5,
	}
	car2 := car{
		Name: "Bantely",
		Year: 4,
	}
	legendsdata := []legends{gandhi, buddha, jesus, gonvindji, mohhamad}
	cardata := []car{car1, car2}
	data := struct {
		Wisdom    []legends
		Transport []car
	}{
		legendsdata, cardata,
	}
	// data := complete{Wisdom: legendsdata, Transport: cardata}
	err := tpl.Execute(os.Stdout, data)
	if err != nil {
		log.Fatal(err)
	}
}
