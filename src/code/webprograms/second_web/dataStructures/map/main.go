package main
import (
	"log"
	"os"
	"text/template"
)
var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("index.gohtml"))
}
func main(){
	var data = map[string]string{
		"Name" : "Pulkit",
		"Graduation" : "B.Tech",
		"Address" : "Jaipur",
		"Company" : "InTimeTec"}
	err := tpl.Execute(os.Stdout,data)
	if err!=nil{
		log.Fatal(err)
	}
}