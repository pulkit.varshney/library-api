package main

import (
	"log"
	"math"
	"os"
	"text/template"
	"time"
)

// var tpl *template.Template
var tpl1 *template.Template

func init() {
	// tpl = template.Must(template.New("").Funcs(fm).ParseFiles("index.gohtml"))
	tpl1 = template.Must(template.New("").Funcs(fm1).ParseFiles("index2.gohtml"))
}

func monthDayYear(t time.Time) string {
	// return t.Format(time.Kitchen)
	return t.Format("01-02-2006")

}

var fm = template.FuncMap{
	"fdateMDY": monthDayYear,
}

func double(x float64) float64 {
	return x + x
}

func square(x float64) float64 {
	return math.Pow(float64(x), 2)
}

func sqRoot(x float64) float64 {
	return math.Sqrt(x)
}

var fm1 = template.FuncMap{
	"fdbl":     double,
	"fsq":      square,
	"fsqrt":    sqRoot,
	"fdateMDY": monthDayYear,
}

type datas struct {
	Value float64
	A     time.Time
}

func main() {

	// err := tpl.ExecuteTemplate(os.Stdout, "index.gohtml", time.Now())
	// if err != nil {
	// 	log.Fatalln(err)
	// }
	data := datas{

		Value: 5.0,
		A:     time.Now(),
	}
	err1 := tpl1.ExecuteTemplate(os.Stdout, "index2.gohtml", data)
	if err1 != nil {
		log.Fatalln(err1)
	}
}
