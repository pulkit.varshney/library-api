package main

import (
	"io"
	"net/http"
)

func main() {
	http.HandleFunc("/", dog)
	http.HandleFunc("/dog.jpg", dogPic)
	http.ListenAndServe(":8080", nil)
}
func dog(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Content-Type", "text/html ;charset=utf-8")
	io.WriteString(res, `<img src="dog.jpg" width=500 height=500> `)
}
func dogPic(res http.ResponseWriter, req *http.Request) {
	http.ServeFile(res, req, "dog.jpg")
}

// package main

// import (
// 	"io"
// 	"net/http"
// )

// func main() {
// 	http.HandleFunc("/", dog)
// 	http.ListenAndServe(":8080", nil)
// }

// func dog(w http.ResponseWriter, req *http.Request) {

// 	w.Header().Set("Content-Type", "text/html; charset=utf-8")

// 	io.WriteString(w, `
// 	<!--not serving from our server-->
// 	<img src="https://upload.wikimedia.org/wikipedia/commons/6/6e/Golde33443.jpg" width=400 height=400>
// 	`)
// }
