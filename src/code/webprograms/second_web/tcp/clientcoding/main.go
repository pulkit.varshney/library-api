package main

import (
	"fmt"
	"log"
	"net"
)

func main() {
	conn, err := net.Dial("tcp", "localhost:8080")
	if err != nil {
		log.Fatalln("errrs")
	}
	defer conn.Close()
	fmt.Fprintln(conn, "I am dialling")
}
