package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"time"
)

func main(){
	li,err:=net.Listen("tcp",":8080")
	if err!=nil{
		log.Fatalln(err)
	}
	defer li.Close()
	for{
		conn,err:=li.Accept()
		if err!=nil{
			log.Fatalln(err)
		}
		// io.WriteString(conn,"Hello I am pulkit")
		// fmt.Fprint(conn,"How are you")
		// conn.Close()
		go handle(conn)
		}
	
}
func handle(conn net.Conn){
	err := conn.SetDeadline(time.Now().Add(10*time.Second))
	if err!=nil{
		log.Fatalln(err)
	}
	scanner := bufio.NewScanner(conn)
	for scanner.Scan(){
		line := scanner.Text()
		fmt.Println(line)
		fmt.Fprintf(conn,"I heard waht you say %s",line)
	}
	defer conn.Close()
	fmt.Println("code got here")
}