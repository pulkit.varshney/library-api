package modules

import (
	"html/template"
	"log"
	"net/http"
)

var tpl *template.Template
var v int

func init() {
	tpl = template.Must(template.ParseGlob("Templates/*"))
}

//ProductDetails ...
func ProductDetails(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "text/html;charset=utf-8")
	err := tpl.ExecuteTemplate(w, "index.gohtml", Data)
	if err != nil {
		log.Fatal(err)
	}
}

//AddToCart ...
func AddToCart(w http.ResponseWriter, req *http.Request) {
	var product Product
	for _, value := range Data {
		if value.ID == req.FormValue("id") {
			product.ID = value.ID
			product.ProductName = value.ProductName
			product.ProductPrice = value.ProductPrice
			product.Productdescription = value.Productdescription
			product.ProductImage = value.ProductImage
		}
	}
	err := tpl.ExecuteTemplate(w, "addToCart.gohtml", product)
	if err != nil {
		log.Fatal(err)
	}

}
