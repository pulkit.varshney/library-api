package modules

//Product ...
type Product struct {
	ID                 string `json:"ID"`
	ProductName        string `json:"name"`
	Productdescription string `json:"designation"`
	ProductPrice       int    `json:"exp"`
	ProductImage       string `json:"salary"`
}

//Data ...
var Data = []Product{
	Product{ID: "1",
		ProductName:        "Trimmer",
		Productdescription: "Philips QT4001/15 cordless rechargeable Beard Trimmer - 10 ",
		ProductPrice:       1199,
		ProductImage:       "/resources/trim.jpg",
	},
	Product{ID: "2",
		ProductName:        "Hair Dryer",
		Productdescription: "Philips HP8100/46 Hair Kera Dryer (Purple)",
		ProductPrice:       775,
		ProductImage:       "/resources/dryer.jpg",
	},
	Product{ID: "3",
		ProductName:        "Straightener",
		Productdescription: "Philips BHS386 Kera Shine Straightener (Purple)",
		ProductPrice:       1955,
		ProductImage:       "/resources/straight.jpg",
	},
	Product{ID: "4",
		ProductName:        "Shampoo &  Hair Straightener",
		Productdescription: "StBotanica Pro Keratin Argan Shampoo + NOVA Hair Straightener",
		ProductPrice:       955,
		ProductImage:       "/resources/shampoo.jpg",
	},
}
