package main

import (
	"code/webprograms/second_web/assignment/modules"
	"net/http"
)

func main() {
	http.Handle("/resources/", http.StripPrefix("/resources", http.FileServer(http.Dir("./assets"))))
	http.HandleFunc("/product", modules.ProductDetails)
	http.HandleFunc("/productcart/", modules.AddToCart)
	http.ListenAndServe(":8080", nil)
}
