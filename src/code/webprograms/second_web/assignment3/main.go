package main

import (
	"html/template"
	"log"
	"net/http"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("index.gohtml"))
}

//Images ...
type Images struct {
	ID          int
	Src         string
	Price       int
	Description string
	ProductName string
}

func index(res http.ResponseWriter, req *http.Request) {
	data := []Images{
		Images{1, "https://image.shutterstock.com/z/stock-photo-handsome-young-bearded-man-trimming-his-beard-with-a-trimmer-762288679.jpg", 1000, "Very Good Product", "Trimmer"},
		Images{2, "Product2.jpg", 2000, "Excellent Product", "Trimmer"},
		Images{3, "Product3.jpg", 3000, "Nice Product", "Trimmer"},
		Images{4, "Product4.jpg", 4000, "Affordable Product", "Trimmer"},
	}
	err := tpl.ExecuteTemplate(res, "index.gohtml", data)
	if err != nil {
		log.Fatal(err)
	}

}
func main() {

	http.HandleFunc("/", index)
	http.ListenAndServe(":8080", nil)
}
