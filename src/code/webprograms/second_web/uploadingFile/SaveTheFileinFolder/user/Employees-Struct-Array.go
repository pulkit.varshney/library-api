package main

import "fmt"

var empArray []employees

type employees struct {
	id            int
	empName       string
	empSal        float32
	contactNumber int
}

//display details of the employee
func (emp *employees) displayDetails() {
	fmt.Println("\nEmployee Id: ", emp.id, "\tEmployee Name: ", emp.empName, "\tEmployee Salary : ", emp.empSal, "\tEmployee Contact Number : ", emp.contactNumber)
	fmt.Println()
}

//Taking details form the user
func scanDetails() {
	var num int
	var (
		id            int
		empName       string
		empSal        float32
		contactNumber int
	)

	fmt.Println("creating array of structures")

	fmt.Println("Enter the number of employees to store data")
	fmt.Scanln(&num)
	var y employees // y is variable of type emplyee struct
	for i := 0; i < num; i++ {
		fmt.Println("Employee Id :")
		fmt.Scanln(&id)
		fmt.Println("Employee Name :")
		fmt.Scanln(&empName)
		fmt.Println("Employee Salary :")
		fmt.Scanln(&empSal)
		fmt.Println("Employee Contact Number :")
		fmt.Scanln(&contactNumber)
		y = employees{id, empName, empSal, contactNumber}
		empArray = append(empArray, y)
	}

}

func main() {

	scanDetails()
	for i := 0; i < len((empArray)); i++ {
		fmt.Println("\nDetails of Employee ", empArray[i].empName)
		empArray[i].displayDetails()
	}

}
