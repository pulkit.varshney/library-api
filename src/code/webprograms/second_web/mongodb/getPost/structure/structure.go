package structure

// Data ...
type Data struct {
	Name   string `json:"Name"`
	Gender string `json:"Gender"`
	Age    string `json:"Age"`
	ID     string `json:"ID"`
}
