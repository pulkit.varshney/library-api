package structure

import (
	"code/webprograms/second_web/mongodb/getPost/structure"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

// UserController ...
type UserController struct{}

// NewUserController ...
func NewUserController() *UserController {
	return &UserController{}
}

// Index ...
func (uc UserController) Index(w http.ResponseWriter, req *http.Request, _ httprouter.Params) {
	s := `<!DOCTYPE html>
			<html lang="en">
			<head>
			<meta charset="UTF-8">
			<title>Index</title>
			</head>
			<body>
			<a href="/user/1">I amusercontroller</a>
			</body>
			</html>
	`
	w.Header().Set("Content-Type", "text/html; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(s))
}

// GetUser ...
func (uc UserController) GetUser(w http.ResponseWriter, req *http.Request, p httprouter.Params) {
	U := structure.Data{
		Name:   "James Bond",
		Gender: "male",
		Age:    "32",
		ID:     p.ByName("id"),
	}
	uj, _ := json.Marshal(U)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s\n", uj)

}

// CreateUser ...
func (uc UserController) CreateUser(w http.ResponseWriter, req *http.Request, _ httprouter.Params) {
	U := structure.Data{
		Name:   "Pulkit",
		Gender: "male",
		Age:    "21",
		ID:     "1",
	}
	json.NewDecoder(req.Body).Decode(&U)
	uj, _ := json.Marshal(U)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s\n", uj)
}
