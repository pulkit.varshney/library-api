package curdstructure

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// UserController ...
type UserController struct {
	session *mgo.Session
}

// NewUserController ...
func NewUserController(s *mgo.Session) *UserController {
	return &UserController{s}
}

// CreateUser ...
func (uc UserController) CreateUser(w http.ResponseWriter, req *http.Request, p httprouter.Params) {
	U := User{}
	json.NewDecoder(req.Body).Decode(&U)
	U.ID = bson.NewObjectId()
	uc.session.DB("products").C("productsInfo").Insert(U)
	uj, _ := json.Marshal(U)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s\n", uj)
}

// GetUser ...
func (uc UserController) GetUser(w http.ResponseWriter, req *http.Request, p httprouter.Params) {
	id := p.ByName("id")
	if !bson.IsObjectIdHex(id) {
		w.WriteHeader(http.StatusNotFound)
	}
	oid := bson.ObjectIdHex(id)
	u := User{}
	if err := uc.session.DB("products").C("productsInfo").FindId(oid).One(&u); err != nil {
		w.WriteHeader(404)
	}
	uj, _ := json.Marshal(u)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s\n", uj)

}
