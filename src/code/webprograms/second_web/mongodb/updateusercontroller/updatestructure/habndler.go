package updatestructure

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2"
)

// UserController ...
type UserController struct {
	session *mgo.Session
}

// NewUserController ...
func NewUserController(s *mgo.Session) *UserController {
	return &UserController{s}
}

// Index ...
func (uc UserController) Index(w http.ResponseWriter, req *http.Request, _ httprouter.Params) {
	s := `<!DOCTYPE html>
			<html lang="en">
			<head>
			<meta charset="UTF-8">
			<title>Index</title>
			</head>
			<body>
			<a href="/user/1">I am mongodb</a>
			</body>
			</html>
	`
	w.Header().Set("Content-Type", "text/html; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(s))
}

// GetUser ...
func (uc UserController) GetUser(w http.ResponseWriter, req *http.Request, p httprouter.Params) {
	U := Data{
		Name:   "James Bond",
		Gender: "male",
		Age:    "32",
		ID:     p.ByName("id"),
	}
	uj, _ := json.Marshal(U)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s\n", uj)

}

// CreateUser ...
func (uc UserController) CreateUser(w http.ResponseWriter, req *http.Request, _ httprouter.Params) {
	U := Data{
		Name:   "Pulkit",
		Gender: "male",
		Age:    "21",
		ID:     "1",
	}
	json.NewDecoder(req.Body).Decode(&U)
	uj, _ := json.Marshal(U)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s\n", uj)
}
