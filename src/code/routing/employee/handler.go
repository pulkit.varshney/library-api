package employee

import (
	"encoding/json"
	"log"
	"net/http"
)

var employees []*Employee

func addEmployee(e *Employee) error {
	db := NewRepository()
	if err := db.Add(e); err != nil {
		return err
	}
	log.Print("Added new Employee")
	return nil
}

// Handler ...
func Handler(writer http.ResponseWriter, request *http.Request) {
	// io.WriteString(writer, "employee routing")
	writer.Header().Set("Content-Type", "application/json")
	switch methodType := request.Method; methodType {
	case "POST":
		var employee Employee
		if err := json.NewDecoder(request.Body).Decode(&employee); err != nil {
			panic("not a valid employee")
		}
		if err := addEmployee(&employee); err != nil {
			json.NewEncoder(writer).Encode(err)
		}
		json.NewEncoder(writer).Encode("added successfully")
	case "GET":
		//json.NewEncoder(writer).Encode(employees)
		if el, err := getEmployee(); err != nil {
			json.NewEncoder(writer).Encode(err)
		} else {
			json.NewEncoder(writer).Encode(el)
		}
	case "DELETE":
	default:
		panic("Route Not Found")
	}

}

func getEmployee() ([]Employee, error) {
	db := NewRepository()
	return db.Get(nil)
}
