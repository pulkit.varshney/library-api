package main

import (
	"code/routing/employee"
	"io"
	"net/http"
)

func hello(writer http.ResponseWriter, request *http.Request) {
	io.WriteString(writer, "Hello World")
}
func pulkit(w http.ResponseWriter, request *http.Request) {
	io.WriteString(w, "Pulkit")
}

var mux map[string]func(w http.ResponseWriter, r *http.Request)

func (*myHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if h, ok := mux[r.URL.String()]; ok {
		h(w, r)
		return
	}
}

type myHandler struct{}

func main() {

	////////Simple Http Setup/////////////////
	// mux := http.NewServeMux()
	// mux.HandleFunc("/", hello)
	// mux.HandleFunc("/pulkit", pulkit)
	// fmt.Printf("Listing on port 7000")
	// http.ListenAndServe(":7000", mux)

	////////Custom Server//////////////////

	// server := http.Server{
	// 	Addr:    ":7000",
	// 	Handler: &myHandler{},
	// }
	// mux = make(map[string]func(w http.ResponseWriter, r *http.Request))
	// mux["/"] = hello
	// mux["/pulkit"] = pulkit
	// server.ListenAndServe()

	////////////Routing///////////////

	server := http.Server{
		Addr:    ":7000",
		Handler: &myHandler{},
	}
	mux = make(map[string]func(w http.ResponseWriter, r *http.Request))
	addHandler("/", hello)
	addHandler("/pulkit", pulkit)
	addHandler("/employee", employee.Handler)
	server.ListenAndServe()
}

func addHandler(path string, handler http.HandlerFunc) {
	if _, ok := mux[path]; ok {
		panic("This path is already exits")
	}
	mux[path] = handler
}
