package main

import (
	"fmt"
	"time"
)

func main() {
	theMine := [5]string{"rock", "ore", "ore", "rock", "ore"}
	oreChannel := make(chan string)
	minedOreChan := make(chan string)

	// Finder
	go func(mine [5]string) {
		for _, item := range mine {
			if item == "ore" {
				oreChannel <- item //send item on oreChannel
			}
		}
	}(theMine)

	// Ore Breaker
	go func() {
		for foundOre := range oreChannel {
			fmt.Println("Miner: Received " + foundOre + " from finder ")
			minedOreChan <- "minedOre"
		}
	}()

	// Smelter
	go func() {
		for minedOre := range minedOreChan {
			fmt.Println("Smelter: Received " + minedOre + " from miner ")
		}
	}()

	<-time.After(time.Second * 5) // Again, you can ignore this

}
